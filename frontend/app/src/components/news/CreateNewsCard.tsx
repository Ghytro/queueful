import React, { useContext } from "react";
import "../styles.css";
import { Button, Form, Input, Spin } from "antd";
import Title from "antd/es/typography/Title";
import tr from "../../config/translation";
import { MessageContext } from "../../App";
import { useNavigate } from "react-router-dom";
import { PlusCircleOutlined } from "@ant-design/icons";
import { CreateNewsRequest, useCreateNewsMutation } from "../../slice/NewsApi";
import TextArea from "antd/es/input/TextArea";

const CreateNewsCard = (): JSX.Element => {
  const messageApi = useContext(MessageContext);
  const navigate = useNavigate();

  const [form] = Form.useForm();
  const [createNews, { isLoading }] = useCreateNewsMutation();

  const submit = (formData: CreateNewsRequest) => {
    createNews(formData)
      .unwrap()
      .then(() => navigate(`/news`))
      .then(() => messageApi.success(tr("News created")))
      .catch(() => messageApi.error(tr("Failed to create news")));
  };

  return (
    <div className="card">
      <Spin spinning={isLoading}>
        <Title level={2}>{tr("New news")}</Title>
        <Form
          form={form}
          layout="vertical"
          requiredMark={false}
          onFinish={(formData: CreateNewsRequest) => submit(formData)}
        >
          <Form.Item
            name={"title"}
            label={tr("Title")}
            rules={[
              {
                required: true,
                message: tr("Please input news title!"),
              },
            ]}
          >
            <Input />
          </Form.Item>
          <Form.Item
            name={"content"}
            label={tr("Content")}
            rules={[
              {
                required: true,
                message: tr("Please input news content!"),
              },
            ]}
          >
            <TextArea />
          </Form.Item>
          <Button
            style={{ width: "100%" }}
            icon={<PlusCircleOutlined />}
            type="primary"
            onClick={() => form.submit()}
          >
            {tr("Publish")}
          </Button>
        </Form>
      </Spin>
    </div>
  );
};
export default CreateNewsCard;
