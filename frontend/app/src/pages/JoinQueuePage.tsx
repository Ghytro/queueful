import React from "react";
import "./styles.css";
import { useSelector } from "react-redux";
import { StorePrototype } from "../config/store";
import NotFoundPage from "./NotFoundPage";
import JoinQueueCard from "../components/queue/JoinQueueCard";

const JoinQueuePage = () => {
  const clientId = useSelector((state: StorePrototype) => state.auth.clientId);
  return clientId ? <JoinQueueCard /> : <NotFoundPage />;
};

export default JoinQueuePage;
