import React from "react";
import "./styles.css";
import NewsListCard from "../components/news/NewsListCard";

const NewsPage = () => {
  return <NewsListCard />;
};

export default NewsPage;
