import React from "react";
import "./styles.css";
import { useSelector } from "react-redux";
import { StorePrototype } from "../config/store";
import CreateNewsCard from "../components/news/CreateNewsCard";
import NotFoundPage from "./NotFoundPage";

const CreateNewsPage = () => {
  const user = useSelector((state: StorePrototype) => state.auth.user);
  return user && user.username === "admin" ? (
    <CreateNewsCard />
  ) : (
    <NotFoundPage />
  );
};

export default CreateNewsPage;
