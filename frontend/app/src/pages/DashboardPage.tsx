import React from "react";
import "./styles.css";
import QueuesList from "../components/queue/QueuesList";
import { useSelector } from "react-redux";
import { StorePrototype } from "../config/store";
import tr from "../config/translation";
import Title from "antd/es/typography/Title";

const DashboardPage = () => {
  const user = useSelector((state: StorePrototype) => state.auth.user);
  return user ? <QueuesList /> : <Title level={2}>{tr("Log in first")}</Title>;
};

export default DashboardPage;
