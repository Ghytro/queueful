import { QuestionCircleOutlined } from "@ant-design/icons";
import React from "react";
import tr from "../config/translation";
import Title from "antd/es/typography/Title";

const NotFoundPage = () => {
  return (
    <>
      <div style={{ width: "100%", marginTop: "3rem" }}>
        <QuestionCircleOutlined style={{ fontSize: "5rem" }} />
      </div>
      <Title>{tr("Page not found")}</Title>
      <Title level={3}>404</Title>
    </>
  );
};

export default NotFoundPage;
