import React from "react";
import "./styles.css";
import { useSelector } from "react-redux";
import { StorePrototype } from "../config/store";
import tr from "../config/translation";
import Title from "antd/es/typography/Title";
import CreateQueueCard from "../components/queue/CreateQueueCard";

const NewQueuePage = () => {
  const user = useSelector((state: StorePrototype) => state.auth.user);
  return user ? (
    <CreateQueueCard />
  ) : (
    <Title level={2}>{tr("Log in first")}</Title>
  );
};

export default NewQueuePage;
