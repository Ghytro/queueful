import React from "react";
import "./styles.css";
import logo from "../../static/logo-full.png";
import { Alert, Button } from "antd";
import tr from "../config/translation";
import { isMobile } from "react-device-detect";
import { WarningOutlined } from "@ant-design/icons";
import { Link } from "react-router-dom";

const MainPage = () => {
  return (
    <div className="card main">
      <img src={logo} alt="logo" className="image" />
      <p style={{ fontSize: "2rem" }}>
        {tr("Queuing has never been so simple")}
      </p>
      <Alert
        type="warning"
        icon={<WarningOutlined />}
        message={tr(
          "BETA! Be aware about loosing ALL your data here, as well as unexpected service shutdowns, bugs and other beta's features :)"
        )}
        showIcon
        style={{ marginBottom: "1rem" }}
      />
      <div className="button-box">
        <Link to="/queue/join">
          <Button size="large">{tr("Join a queue")}</Button>
        </Link>
        {!(
          isMobile && window.screen.orientation.type === "portrait-primary"
        ) && <div style={{ width: "3rem" }} />}
        <Button size="large">{tr("Take a tour")}</Button>
      </div>
    </div>
  );
};

export default MainPage;
