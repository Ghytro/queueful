import React, { ReactNode } from "react";
import { useSelector } from "react-redux";
import { StorePrototype } from "./store";
import { ConfigProvider, Spin } from "antd";
import { darkTheme, lightTheme } from "./style";
import PropTypes from "prop-types";
import { LoadingOutlined } from "@ant-design/icons";

const ThemeProviderWrapper = ({ children }: { children: ReactNode }) => {
  const theme = useSelector((state: StorePrototype) => state.settings.theme);
  Spin.setDefaultIndicator(<LoadingOutlined style={{ fontSize: 36 }} spin />);

  return (
    <ConfigProvider theme={theme === "dark" ? darkTheme : lightTheme}>
      {children}
    </ConfigProvider>
  );
};

ThemeProviderWrapper.propTypes = {
  children: PropTypes.node,
};

export default ThemeProviderWrapper;
