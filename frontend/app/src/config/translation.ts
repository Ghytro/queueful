import trMapJson from "./translationMap.json";
import { store } from "./store";

const trMap: unknown = trMapJson;

const tr = (phrase: string): string => {
  const currentLanguage = store.getState().settings.language;
  if (!currentLanguage || currentLanguage === "en") {
    return phrase;
  }
  return (
    ((trMap &&
      typeof trMap === "object" &&
      trMap[phrase as keyof object] &&
      trMap[phrase as keyof object][
        currentLanguage as keyof object
      ]) as string) || phrase + ` (${currentLanguage}?)`
  );
};

export default tr;
