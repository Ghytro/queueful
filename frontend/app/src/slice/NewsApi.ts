import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";
import { baseUrl } from "../config/baseUrl";
import { RootState } from "../config/store";

export type CreateNewsRequest = {
  title: string;
  content: string | null;
};

export type News = {
  id: string;
  title: string;
  content: string;
  created: string;
  taps: number;
};

export const NewsApi = createApi({
  reducerPath: "NewsApi",
  baseQuery: fetchBaseQuery({
    baseUrl: `${baseUrl}/news`,
    prepareHeaders: (headers, { getState }) => {
      const token = (getState() as RootState).auth.token;
      if (token) {
        headers.set("authorization", `Bearer ${token}`);
      }
      return headers;
    },
  }),
  endpoints: (builder) => ({
    getNews: builder.query({
      query: () => "/",
    }),
    tapNews: builder.mutation({
      query: (newsId: string) => ({
        url: `/${newsId}/tap`,
        method: "POST",
      }),
    }),
    createNews: builder.mutation({
      query: (data: CreateNewsRequest) => ({
        url: "/",
        method: "POST",
        body: data,
      }),
    }),
  }),
});

export const { useGetNewsQuery, useTapNewsMutation, useCreateNewsMutation } =
  NewsApi;
