import React, { createContext } from "react";
import { message } from "antd";
import "./App.css";
import HeaderComponent from "./components/HeaderComponent";
import { Provider } from "react-redux";
import { store } from "./config/store";
import { MessageInstance } from "antd/es/message/interface";
import AppRoutes from "./pages/AppRoutes";
import ThemeProviderWrapper from "./config/ThemeProviderWrapper";

export const MessageContext = createContext({} as MessageInstance);

const App = () => {
  const [messageApi, contextHolder] = message.useMessage({
    duration: 2,
  });

  return (
    <Provider store={store}>
      <ThemeProviderWrapper>
        <MessageContext.Provider value={messageApi}>
          <div className="content">
            {contextHolder}
            <AppRoutes>
              <HeaderComponent />
            </AppRoutes>
          </div>
        </MessageContext.Provider>
      </ThemeProviderWrapper>
    </Provider>
  );
};

export default App;
