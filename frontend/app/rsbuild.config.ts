import { defineConfig } from "@rsbuild/core";
import { pluginReact } from "@rsbuild/plugin-react";

export default defineConfig({
  plugins: [pluginReact()],
  server: {
    host: "0.0.0.0",
    port: 3000,
  },
  html: {
    favicon: "./static/favicon-32x32.png",
    appIcon: "./static/android-chrome-512x512.png",
    title: "queueful!",
    template: "./static/index.html",
  },
});
