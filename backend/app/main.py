from typing import Union, Annotated
from fastapi import FastAPI, Depends
import redis

from .db import models
from .db.database import SessionLocal, engine
from .db.redis import create_redis
from .dependencies import get_db

from .views.auth.api import router as auth_router
from .views.queue.api import router as queue_router
from .views.news.api import router as news_router

app = FastAPI(dependencies=[Depends(get_db)])
models.Base.metadata.create_all(bind=engine)

app.include_router(queue_router)
app.include_router(auth_router)
app.include_router(news_router)


@app.on_event("startup")
async def startup_event():
    r = await create_redis()
    await r.flushall()


@app.get("/")
async def read_root():
    return {"message": "OK"}
