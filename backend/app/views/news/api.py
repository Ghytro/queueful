from datetime import datetime, timedelta, timezone
from typing import Annotated, Union
from sqlalchemy.orm import Session
from uuid import UUID

from fastapi import APIRouter, Depends, HTTPException, status
from fastapi.security import OAuth2PasswordRequestForm


from pydantic import BaseModel

from ...config import jwt_config
from ...dependencies import get_db
from . import schemas
from . import services

from ..auth import services as auth_services
from ..auth import schemas as auth_schemas


router = APIRouter(
    prefix="/news",
    tags=["news"],
    dependencies=[Depends(get_db)],
    responses={404: {"description": "Not found"}},
)


@router.get("/")
async def get_news(
    news: Annotated[schemas.NewsInDb, Depends(services.get_news)],
) -> list[schemas.NewsInDb]:
    return news


@router.post("/")
async def create_news(
    news: schemas.CreateNews,
    current_user: Annotated[auth_schemas.User, Depends(auth_services.get_current_user)],
    db: Annotated[Session, Depends(get_db)],
) -> schemas.NewsInDb:
    return services.create_news(news=news, current_user=current_user, db=db)


@router.post("/{news_id}/tap")
async def create_news(
    news_id: UUID,
    db: Annotated[Session, Depends(get_db)],
) -> schemas.TapResponse:
    services.tap_news(news_id=news_id, db=db)
    return schemas.TapResponse(tap="ok")
