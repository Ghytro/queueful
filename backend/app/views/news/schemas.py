from typing import Union
from pydantic import BaseModel
from uuid import UUID
from datetime import datetime


class CreateNews(BaseModel):
    title: str
    content: str


class News(BaseModel):
    title: str
    content: str
    created: datetime
    taps: int


class NewsInDb(News):
    id: UUID

    class Config:
        from_attributes = True


class TapResponse(BaseModel):
    tap: str
