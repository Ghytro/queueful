from datetime import datetime, timedelta, timezone
from typing import Annotated, Union
from sqlalchemy.orm import Session
import redis

from fastapi import APIRouter, Depends, HTTPException, status
from fastapi.security import OAuth2PasswordRequestForm


from pydantic import BaseModel

from ...config import jwt_config
from ...dependencies import get_db, get_redis
from ...db import models
from . import schemas
from . import services

from ..auth import services as auth_services
from ..auth import schemas as auth_schemas


router = APIRouter(
    prefix="/queue",
    tags=["queue"],
    dependencies=[Depends(get_db)],
    responses={404: {"description": "Not found"}},
)


@router.get("/owned")
async def owned_queues_list(
    queues: Annotated[schemas.Queue, Depends(services.get_owned_queues)],
) -> list[schemas.QueueInDb]:
    return queues


@router.get("/")
async def anonuser_queues_list(
    queues: Annotated[schemas.Queue, Depends(services.get_user_queues)],
) -> list[schemas.QueueInDb]:
    return queues


@router.get("/{queue_id}")
async def user_queues_list(
    queue: Annotated[schemas.QueueDetail, Depends(services.get_detailed_queue)],
) -> schemas.QueueDetail:
    return queue


@router.post("/")
async def create_queue(
    new_queue: schemas.Queue,
    current_user: Annotated[auth_schemas.User, Depends(auth_services.get_current_user)],
    db: Annotated[Session, Depends(get_db)],
) -> schemas.QueueInDb:
    return services.create_queue(new_queue=new_queue, current_user=current_user, db=db)


@router.post("/{queue_id}/join")
async def join_queue(
    queue_user: Annotated[schemas.QueueUser, Depends(services.join_queue)]
) -> schemas.QueueUser:
    return queue_user


@router.post("/{queue_id}/listen")
async def listen_queue(
    updated_queue: Annotated[schemas.QueueDetail, Depends(services.set_queue_listener)]
) -> schemas.QueueDetail:
    return updated_queue


@router.post("/{queue_id}/action/{action}")
async def perform_queue_action(
    result: Annotated[schemas.ActionResult, Depends(services.action_wrapper)]
):
    return result
