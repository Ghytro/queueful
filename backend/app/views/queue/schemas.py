from typing import Union, List
from pydantic import BaseModel
from uuid import UUID
from ..auth import schemas as auth_schemas


class QueueUser(BaseModel):
    id: UUID
    position: int
    passed: bool
    user: auth_schemas.AnonUser

    class Config:
        from_attributes = True


class ParticipantInfo(BaseModel):
    total: int
    remaining: int
    users_list: List[QueueUser]


class Queue(BaseModel):
    name: str
    description: Union[str, None] = None


class QueueInList(Queue):
    participants: ParticipantInfo

    class Config:
        from_attributes = True


class QueueInDb(Queue):
    id: UUID

    class Config:
        from_attributes = True


class QueueDetail(Queue):
    id: UUID
    status: str
    owner_id: UUID
    participants: ParticipantInfo


class ActionResult(BaseModel):
    action: str
    result: str

    class Config:
        from_attributes = True
