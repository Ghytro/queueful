from typing import Annotated
from fastapi import Depends
import redis

from .db.database import SessionLocal
from .db.redis import create_redis


def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()


async def get_redis():
    r = await create_redis()
    try:
        yield r
    finally:
        r.close()


async def get_pubsub(r: Annotated[redis.Redis, Depends(get_redis)]):
    ps = r.pubsub()
    try:
        yield ps
    finally:
        ps.close()
